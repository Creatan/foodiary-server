import chai from 'chai';
import chaiHttp from 'chai-http';
import mongoose from 'mongoose';

import server from '../../src/app';
import Food from '../../src/models/food';

let should = chai.should();

chai.use(chaiHttp);

describe('FOODS',() => {
  Food.collection.drop();

  let mockFood = {
    name: 'Gourmet Karamelli',
    brand: 'Rainbow',
    unit: 'g',
    amount: 100,
    calories: 230,
    proteins: 2.6,
    fat: 11,
    carbs: 31
  };

  let mockTestFood = {
    name: 'Makaroni',
    brand: 'Myllynparas',
    amount: 100,
    unit: 'g',
    calories: 348,
    proteins: 13,
    fat: 1.6,
    carbs: 68
  };

  beforeEach(done => {
      let testFood = new Food(mockTestFood);

      testFood.save(err => {
        done();
      });
  });
  afterEach(done => {
    Food.collection.drop();
    done();
  });

  it('should list ALL foods on /foods GET',done => {
    chai.request(server)
        .get('/foods')
        .end((err, res) => {
          res.should.have.status(200);
          res.should.be.json;
          res.body.should.be.a('array');
          res.body[0].name.should.equal('Makaroni');
          done();
        });
  });

  it('should list a SINGLE food on /foods/:id GET', done => {
    let newFood = new Food(mockFood);
    newFood.save((err,data) => {
      chai.request(server)
          .get('/foods/'+data.id)
          .end((err, res) => {
            res.should.have.status(200);
            res.should.be.json;
            res.body.should.be.a('object');
            res.body.should.have.property('_id');
            res.body.should.have.property('name');
            res.body.should.have.property('brand');
            res.body.should.have.property('amount');
            res.body.should.have.property('unit');
            res.body.should.have.property('calories');
            res.body.should.have.property('proteins');
            res.body.should.have.property('fat');
            res.body.should.have.property('carbs');

            res.body.name.should.equal(mockFood.name);
            res.body.brand.should.equal(mockFood.brand);
            res.body.amount.should.equal(mockFood.amount);
            res.body.unit.should.equal(mockFood.unit);
            res.body.calories.should.equal(mockFood.calories);
            res.body.proteins.should.equal(mockFood.proteins);
            res.body.fat.should.equal(mockFood.fat);
            res.body.carbs.should.equal(mockFood.carbs);
            res.body._id.should.equal(data.id);

            done();
          });
    });
  });
  it('should return 404 on /foods/:id GET if id is invalid ObjectId', done => {
    chai.request(server)
        .get('/foods/abc')
        .end((err, res) => {
          res.should.have.status(404);
          done();
        });
  });
  it('should return 404 on /foods/:id GET if food is not found by id', done => {
    chai.request(server)
        .get('/foods/507f191e810c19729de860ea')
        .end((err, res) => {
          res.should.have.status(404);
          done();
        });
  });
  it('should add a SINGLE food on /foods POST', done => {
      chai.request(server)
          .post('/foods')
          .send(mockFood)
          .end((err,res) => {
            res.should.have.status(200);
            res.should.be.json;
            res.body.should.be.a('object');
            res.body.should.have.property('SUCCESS');
            res.body.SUCCESS.should.be.a('object');
            res.body.SUCCESS.should.have.property('name');
            res.body.SUCCESS.should.have.property('_id');
            res.body.SUCCESS.should.have.property('brand');
            res.body.SUCCESS.name.should.equal('Gourmet Karamelli');
            res.body.SUCCESS.brand.should.equal('Rainbow');
            done();
          });
  });
  it('should give an error if there are required fields missing on /foods POST', done => {
      delete mockFood.calories;
      chai.request(server)
          .post('/foods')
          .send(mockFood)
          .end((err, res) => {
            res.should.have.status(500);
            res.should.be.json;
            res.body.should.be.a('object');
            res.body.should.have.property('ERROR');
            res.body.ERROR.should.be.a('object');
            res.body.ERROR.should.have.property('errors');
            res.body.ERROR.errors.should.have.property('calories');
            res.body.ERROR.errors.calories.properties.type.should.equal('required');

            done();
          });
  });
  it('should update a SINGLE food on /foods/:id PUT', done => {
    chai.request(server)
        .get('/foods')
        .end((err, res) => {
          chai.request(server)
              .put('/foods/'+res.body[0]._id)
              .send({name:'Spagetti', brand:'Eldorado',calories:1000})
              .end((error, response) => {
                response.should.have.status(200);
                response.should.be.json;
                response.body.should.be.a('object');
                response.body.should.have.property('UPDATED');
                response.body.UPDATED.should.have.property('_id');
                response.body.UPDATED.should.have.property('name');
                response.body.UPDATED.should.have.property('brand');
                response.body.UPDATED.should.have.property('amount');
                response.body.UPDATED.should.have.property('unit');
                response.body.UPDATED.should.have.property('calories');
                response.body.UPDATED.should.have.property('proteins');
                response.body.UPDATED.should.have.property('fat');
                response.body.UPDATED.should.have.property('carbs');

                response.body.UPDATED.name.should.equal('Spagetti');
                response.body.UPDATED.brand.should.equal('Eldorado');
                response.body.UPDATED.amount.should.equal(mockTestFood.amount);
                response.body.UPDATED.unit.should.equal(mockTestFood.unit);
                response.body.UPDATED.calories.should.equal(1000);
                response.body.UPDATED.proteins.should.equal(mockTestFood.proteins);
                response.body.UPDATED.fat.should.equal(mockTestFood.fat);
                response.body.UPDATED.carbs.should.equal(mockTestFood.carbs);
                response.body.UPDATED._id.should.equal(res.body[0]._id);
                done();
              });
        });
  });
  it('should delete a SINGLE food on /foods/:id DELETE', done => {
    chai.request(server)
        .get('/foods')
        .end((err, res) => {
          chai.request(server)
              .delete('/foods/'+res.body[0]._id)
              .end((error,response) => {
                  response.should.have.status(200);
                  response.should.be.json;
                  response.body.should.be.a('object');
                  response.body.should.have.property('REMOVED');
                  response.body.REMOVED.should.equal(res.body[0]._id);
                  done();
              });

        });
  });

  after(() => {
    mongoose.connection.close();
  });
});
