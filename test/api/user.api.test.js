/* jshint expr: true */
process.env.NODE_ENV = 'test';

import chai from 'chai';
import chaiHttp from 'chai-http';

import server from '../../src/app';
import User from '../../src/models/user';

let should = chai.should();

chai.use(chaiHttp);

describe.only('USERS', () => {

  User.collection.drop();

  const mockUser = {
    username: 'Admin',
    password: 'hunter2'
  };

  beforeEach(done => {
    let user = new User(mockUser);

    user.save(err => {
      done();
    });
  });

  afterEach(done => {
    User.collection.drop();
    done();
  });

  it('should add an user on /register POST', done => {
    let testUser = {
      username: 'User',
      password: 'testPassword'
    };

    chai.request(server)
        .post('/register')
        .send(testUser)
        .end((err, res) => {
          res.should.have.status(200);
          res.should.be.json;

          done();
        });
  });
  it('should return xxx and error message if user exists on /register POST');

  it('should return jwt token on successfull /authenticate POST');

  it('should return 401 on invalid /authenticate POST', done => {
    chai.request(server)
        .post('/login')
        .send({username:'InvalidUser',password:'InvalidPassword'})
        .end((err, res) => {
          res.should.have.status(401);
          done();
        });
  });
  it('should update user info on /user/:id PUT');
});
