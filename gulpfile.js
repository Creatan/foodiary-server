var gulp = require('gulp');
var mocha = require('gulp-mocha');
var babel = require('babel-core/register');

// if running CI consider changing the error handling to be similar to
// http://stackoverflow.com/a/21735066
gulp.task('test', function() {
  return gulp.src('test/**/*.js')
    .pipe(mocha({
      compilers: {
        js: babel
      }
    }))
    .on('error',function(err){
      // gulp-mocha throws an error when test fails
      // so we ignore it and display errors that come from other plugins
      // we emit end so that we can execute the test in watch
      if(err.plugin !== 'gulp-mocha'){
          console.log(err.toString());
      }
      this.emit('end');
    });
});

gulp.task('default', ['test'], function() {
  gulp.watch(['src/**/*.js', 'test/**/*.js'], ['test']);
});
