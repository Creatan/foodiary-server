import jwt from 'jsonwebtoken';

/**
 * Authentication middleware based on jsonwebtoken
 * requires secret to be passed in a options object
 * additionally you can pass url paths as an array that you want to skip the authentication for
 * token can be passed via query string, post or authorization header
 * sets req.user to be the decoded userinformation
 */

export default (options) => {
  let opts = options || {};
  let paths = opts.paths || [];

  return (req, res, next) => {
    if (paths.indexOf(req.path) > -1) {
      return next();
    }

    let token = req.body.token || req.query.token || req.headers.authorization;

    if (token) {
      jwt.verify(token, opts.secret, (err, decoded) => {
        if (err) {
          res.status(401).json({
            'error': 'Invalid authentication token'
          });
        } else {
          req.user = decoded;
          return next();
        }
      });
    } else {
      res.status(401).json({
        'error': 'Authentication token missing'
      });
    }
  };
};
