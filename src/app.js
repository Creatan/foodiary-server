import express from 'express';
import bodyParser from 'body-parser';
import mongoose from 'mongoose';
import P from 'bluebird'

import jwtAuth from './jwt-auth';
import config from './config';

// ROUTES
import foods from './routes/food';
import user from './routes/user';

let app = express();

// Middleware setup
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({
  extended: false
}));

// Database connection setup
mongoose.connect(config.dbURI[process.env.NODE_ENV], (err, res) => {
  if (err) {
    console.log('Error connecting to the database: ' + err);
  }
});
mongoose.Promise = P;

app.use(jwtAuth({
  secret: config.secret,
  paths: ['/register', '/authenticate']
}));
app.use('/', user);
app.use('/foods', foods);


// don't start up the server if app is imported
if (!module.parent) {
  app.listen(process.env.PORT || 3000)
}

export default app;
