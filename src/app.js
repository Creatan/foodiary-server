import express from 'express';
import bodyParser from 'body-parser';
import mongoose from 'mongoose';
import http from 'http';

import jwtAuth from './jwt-auth';
import config from './config';

// ROUTES
import foods from './routes/food';
import user from './routes/user';

let app = express();

// Middleware setup
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({
  extended: false
}));

// Database connection setup
mongoose.connect(config.dbURI[app.settings.env], (err, res) => {
  if (err) {
    console.log('Error connecting to the database: ' + err);
  }
});

app.use(jwtAuth({
  secret: config.secret,
  paths: ['/register', '/authenticate']
}));
app.use('/', user);
app.use('/foods', foods);


// don't start up the server if app is imported
if (!module.parent) {
  let server = http.createServer(app);
  server.listen(3000, () => {
    console.log('Foodiary-api server running on http://localhost:3000');
  });
}

export default app;
