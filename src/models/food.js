import mongoose from 'mongoose';

let foodSchema = new mongoose.Schema({
  name: { type: String, required: true },
  brand: { type: String, required: true },
  amount: { type: Number, required: true },
  unit: { type: String, required: true },
  calories: { type: Number, required: true },
  proteins: { type: Number, required: true },
  fat: { type: Number, required: true },
  carbs: { type: Number, required: true }
});

export default mongoose.model('foods', foodSchema);
