import mongoose from 'mongoose';

let userSchema = new mongoose.Schema({
  username: String,
  password: String  
});

export default mongoose.model('users', userSchema);
