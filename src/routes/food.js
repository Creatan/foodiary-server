import express from 'express';
import mongoose from 'mongoose';

import Food from '../models/food';

let router = express.Router();

// Move the route functions into their own file?

// helper function for mapping request data to a model
// if replace is true the model is an actual mongoose model
// and we want to replace the values and return the model back for saving
function mapData(body, model, replace = false){
  let fields = model.schema.paths;

  if(replace === false){
    model = {};
  }

  Object.keys(fields).forEach(key => {
    if(typeof body[key] !== 'undefined'){
      model[key] = body[key];
    }
  });

  return model;
}

// TODO: remove the unnessessary response object wrapping from responses

function getAllFoods(req, res){
  Food.find((err, foods) => {
    if(err){
      res.json({'ERROR':err});
    }
    else{
      res.json(foods);
    }
  });
}

function addNewFood(req, res){
  var newFood = new Food(mapData(req.body, Food));

  newFood.save(err => {
    if(err){
      // error is mostly due to schema validation
      res.status(500).json({'ERROR':err});
    }
    else{
      res.json({'SUCCESS':newFood});
    }
  });
}

function getFoodById(req, res){
  // return 404 if requested id is not a valid ObjectId
  // TODO: change into proper error message
  if(mongoose.Types.ObjectId.isValid(req.params.id) === false){
    res.status(404);
  }

  Food.findById(req.params.id, (err, food) => {
    if(err){
      res.json({'ERROR':err});
    }
    else{
      // return 404 if we haven't found the food by id
      if(food === null){
        res.status(404);
      }

      res.json(food);
    }
  });
}

function updateFood(req, res){
  Food.findById(req.params.id, (err, food) => {
    food = mapData(req.body, food, true);
    food.save(err => {
      if(err){
        res.json({'ERROR':err});
      }
      else{
        res.json({'UPDATED':food});
      }
    });
  });
}

function deleteFood(req, res){
  Food.findById(req.params.id, (err, food) => {
    if(err){
      res.json({'ERROR': err});
    }
    else{
      food.remove(err => {
        if(err){
          res.json({'ERROR':err});
        }
        else{
          res.json({'REMOVED':food._id});
        }
      });
    }
  });
}


// these url are relative to the url defined in app.js
// app.js app.use('/food',router)
router.get('/',getAllFoods);
router.post('/',addNewFood);
router.get('/:id',getFoodById);
router.put('/:id',updateFood);
router.delete('/:id',deleteFood);

export default router;
