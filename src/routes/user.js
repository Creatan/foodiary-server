import express from 'express';

import User from '../models/user';

let router = express.Router();

function registerUser(req, res) {
  let newUser = new User({
    username: req.body.username,
    password: req.body.password
  });

  newUser.save((err, user) => {
    if (err) {
      res.status(500).json({
        'error': err
      });
    } else {
      res.json(user);
    }
  });
}

router.post('/register', registerUser);


export default router;
